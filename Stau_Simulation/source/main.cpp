﻿//
//  main.cpp
//  Stau_Simulation
//
// Diese Simulation soll einen Elementaren Zellulären Automaten mittels des
// Wolfram‘s Universums erzeugen um einen Stau Grafisch Simulieren zu können 
//
//  Created by Lukas Branigan on 07/03/14.
//  Copyright (c) 2014 Lukas Branigan. All rights reserved.
//

#include <iostream>
#include "Strasse.h"
#include <vector>

using namespace std;

int main(int argc, const char * argv[])
{
    Strasse s;

	//Startaufstellung der Strasse
    int street[SIZE-2] = {1,1,0,1,0,1,1,1,0,1,1,1,0,0,1,1,0,1};
    
    s.setStreet(street,22);
    s.compute();
    
	system("PAUSE");

    return 0;
}
