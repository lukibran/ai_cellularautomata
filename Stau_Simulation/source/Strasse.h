//
//  Strasse.h
//  Stau_Simulation
//
//  Created by Lukas Branigan on 07/03/14.
//  Copyright (c) 2014 Lukas Branigan. All rights reserved.
//

#ifndef __Stau_Simulation__Strasse__
#define __Stau_Simulation__Strasse__

#include <iostream>
#include <vector>

#define SIZE 20

class Strasse
{
public:
    
    Strasse();
	~Strasse();
    void setStreet(int s[], int generations);
    void printStreet();
    void compute();
    
private:
    int* m_zustand1;
    int* m_zustand2;
    int m_gen;
    
};

#endif /* defined(__Stau_Simulation__Strasse__) */

