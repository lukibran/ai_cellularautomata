//
//  Strasse.cpp
//  Stau_Simulation
//
//  Created by Lukas Branigan on 07/03/14.
//  Copyright (c) 2014 Lukas Branigan. All rights reserved.
//

#include "Strasse.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;


Strasse::Strasse()
{
	//Allokieren der Arrays zwischen denen geswitched wird
	m_zustand1 = new int[SIZE];
	m_zustand2 = new int[SIZE];
	for(int i=0; i < SIZE;i++)
	{
		m_zustand1[i] = 0;
		m_zustand1[i] = 0;
	}
}

Strasse::~Strasse()
{
	//Deallokieren der Arrays
	delete [] m_zustand1;
	delete [] m_zustand2;
}


void Strasse::setStreet(int s[], int generations)
{
	//erste und letzte Stelle des Arrays wird bestimmt
    m_zustand1[0] = 0; //(rand() / (RAND_MAX)) + 1;
    for(int i=1; i<SIZE-1; i++)
    {
		m_zustand1[i] = s[i-1];
    }

	//Letzte Stelle auf Null damit kein Kompletter Stau auftritt
    m_zustand1[SIZE-1] = 0;

	
    
    // Generationen auf gen schreiben
    m_gen = generations;
}

void Strasse::printStreet()
{
	//Ausgabe des der Stausimulation
	// Kein Auto ist . 
	// Auto ist *
    for(int i=1; i<SIZE-1; i++)
    {
      
		if(m_zustand1[i] == 0)
		{
			cout << ".";
		}
		else
		{
			cout << "*";
		}
    }
	cout << endl;
}

void Strasse::compute()
{
    int round = 0;
    int nr = 0;
	int count = 0;
    
    //Erste und Letzte Stelle randomiziert einf�gen
    srand((unsigned)time(NULL));

	for(int i=0;i<SIZE;i++)
	{
		m_zustand2[i]=m_zustand1[i];
	}

    while(round < m_gen)
    {
        // Schleife zum durchgehen der einzelnen Elemente des Arrays
        for(int i=1; i<SIZE-1; i++)
        {
          
			//Count je nach Zustand der Nachbarn erh�hen Max 111
			if(m_zustand1[i-1] == 1)
			{
				count += 100;
			}
			if(m_zustand1[i] == 1)
			{
				count += 10;
			}
			if(m_zustand1[i+1] == 1)
			{
				count += 1;
			}
            
			//Abfrage auf alle verschiedenen Zust�nde
			switch(count)
			{
				case 0:
					m_zustand2[i] = 0;
					break;
				case 1:
					m_zustand2[i] = 0;
					break;
				case 10:
					m_zustand2[i] = 0;
					break;
				case 11:
					m_zustand2[i] = 1;
					break;
				case 100:
					m_zustand2[i] = 1;
					break;
				case 101:
					m_zustand2[i] = 1;
					break;
				case 110:
					m_zustand2[i] = 0;
					break;
				case 111:
					m_zustand2[i] = 1;
					break;
				default:
					break;
			}
				
			count = 0;
			
		}
	
	//Wird letzte Stelle 1 wird hier wieder auf 0 gesetzt 
	//so werden die weiterfahrenden Auto simuliert kann kein kompletter Stau auftreten
	if(m_zustand2[SIZE-1] == 1)
	{
		m_zustand2[SIZE-1] = 0;
	}

	//Der ver�nderte Zustand2 wird wieder auf Zustand1 geswitched
	for(int i=0;i<SIZE;i++)
	{
		m_zustand1[i]=m_zustand2[i];
	}    

	//Ausgabe der Stra�e so oft wie Generationen eingestellt wurden
    printStreet();
    round++;
    }
}  