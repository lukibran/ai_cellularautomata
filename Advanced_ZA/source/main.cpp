#include "precomp.h"
#include "Sanduhr.h"

#include <ClanLib/gl.h>

int main(const std::vector<std::string> &args)
{

	// Initialize ClanLib base components
	clan::SetupCore setup_core;

	// Initialize the ClanLib display component
	clan::SetupDisplay setup_display;
	clan::SetupGL setup_gl;


	// Start the Application
	Sanduhr app;
	int retval = app.start(args);
	return retval;
}

// Instantiate CL_ClanApplication, informing it where the Program is located
clan::Application app(main);