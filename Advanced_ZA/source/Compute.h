#pragma once

#include "precomp.h"

#define WIDTH 265
#define HEIGHT 530

class Compute
{
public:
	Compute(void);
	~Compute(void);
	int initialUhr(); //Initialisierung der StartAnsicht der Sanduhr im Array
	int printUhr(); //TestFunktion zum Ausgeben des Arrays auf der Konsole
	int** calcStep(); //Ver�nderung der Standk�rner mittels Regeln abfragen

private:
	int** m_board;
	int m_row;
	int m_col;

	//Pointer um durch die Arrays durchgehen zu k�nnen und Nachbarn vergleichen zu k�nnen
	int* p_lefttop;
	int* p_leftbot;
	int* p_righttop;
	int* p_rightbot;
};

