#include "Sanduhr.h"

using namespace std;

Sanduhr::Sanduhr(void)
{
	board = new int *[HEIGHT];
	for(int i=0;i<HEIGHT;i++)
	{
		board[i] = new int[WIDTH];
	}
}

Sanduhr::~Sanduhr(void)
{
	for(int i=0;i<WIDTH;i++)
	{
		delete[] board[i];
	}
	delete [] board;
}

int Sanduhr::start(const std::vector<std::string> &args)
{
	quit = false;
	
	//Initialisierung von Clanlib und Fenster
	clan::DisplayWindowDescription desc;
	desc.set_title("Sanduhr");
	desc.set_size(clan::Size(WIDTH, HEIGHT), true);
	desc.set_allow_resize(false);

	clan::DisplayWindow window(desc);
	clan::Canvas canvas(window);

	clan::InputContext ic = window.get_ic();
	clan::InputDevice keyboard = ic.get_keyboard();
	clan::InputDevice mouse = ic.get_mouse();
	clan::GraphicContext gc = window.get_gc();

	int last_time = clan::System::get_time();
	
	Compute m_sand;
	srand(time(NULL));

	m_sand.initialUhr();	
	
	while(!quit)
	{
		canvas.clear(clan::Colorf::white);
		
		
		//Hier wird nach jedem durchgang die Methode CalcStep aufgerufen und das 
		//Ergebnis in board gespeichert um dieses ausgeben zu k�nnen
		board = m_sand.calcStep();

		//Ausgabe des Arrays mitts einzelner Pixel
		for(int i=0;i<HEIGHT; i++)
		{
			for(int j=0; j<WIDTH; j++)
			{	
				if(board[i][j] == 0)
				{
					canvas.draw_point(j,i,clan::Colorf::aliceblue);
				}
				if(board[i][j] == 1)
				{
					canvas.draw_point(j,i,clan::Colorf::black);
				}
				if(board[i][j] == 2)
				{
					canvas.draw_point(j,i,clan::Colorf::beige);
				}
				//std::cout << board[i][j];
			}
		//std::cout << std::endl;
		}	
		
		canvas.flush();
		
		window.flip();
		clan::KeepAlive::process(0);
		
	}

	return 0;
}

void Sanduhr::on_input_up(const clan::InputEvent &key)
{
	if(key.id == clan::keycode_escape)
	{
		quit = true;
	}
}

void Sanduhr::on_window_close()
{
	quit = true;
}