#pragma once

#include "precomp.h"
#include "Compute.h"

#define WIDTH 265
#define HEIGHT 530

class Sanduhr
{
public:
	Sanduhr(void);
	~Sanduhr(void);

	int start(const std::vector<std::string> &args);

private:
	void on_input_up(const clan::InputEvent &key);
	void on_window_close();
	bool quit;
	int** board;
};
