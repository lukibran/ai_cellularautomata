#include "Compute.h"
#include <iostream>

Compute::Compute(void)
{
	m_board = new int *[HEIGHT];
	for(int i=0;i<HEIGHT;i++)
	{
		m_board[i] = new int[WIDTH];
	}

	p_lefttop = 0;
	p_leftbot = 0;
	p_righttop = 0;
	p_rightbot = 0;
}


Compute::~Compute(void)
{
	for(int i=0;i<WIDTH;i++)
	{
		delete[] m_board[i];
	}
	delete [] m_board;
}

int Compute::initialUhr()
{
	int pos=0;

	//Ganzes Array mit 0 Initialisieren
	for(int i=0;i<HEIGHT; i++)
	{
		for(int j=0; j<WIDTH; j++)
		{	
			m_board[i][j] = 0;
		}	
	}

	
	//Linke Randlinien der Sanduhr im Array Initialisieren mit 1
	for (int i=0; i<HEIGHT; i++)
	{
		if(i<260)
		{
			m_board[i][pos] = 1;
			if(i%2)
			{
				pos++;
			}	
		}
		else
		{
			
			if(i%2)
			{
				pos--;
			}
			m_board[i][pos] = 1;
			
		}		
	}

	//Rechte Randlinien der Sanduhr im Array Initialisieren mit 1
	pos = WIDTH-1;

	for (int i=0; i<HEIGHT; i++)
	{
		if(i<260)
		{
			m_board[i][pos] = 1;
			if(i%2)
			{
				pos--;
			}	
		}
		else
		{
			
			if(i%2)
			{
				pos++;
			}
			m_board[i][pos] = 1;
			
		}		
	}

	//Oberes Ende und Unteres Ende Initialiesieren mit 1
	for(int i=0; i<WIDTH; i++)
	{
		m_board[0][i] = 1;
		m_board[HEIGHT-1][i] = 1;
	}


	//Sand in der Uhr Initialisieren mit 2
	int posstart = 1;
	int posend = WIDTH-1;

	for(int i=1; i<(HEIGHT-1)/2; i++)
	{
		int j=0;
		for(j=posstart; j<posend; j++)
		{
			m_board[i][j] = 2;
				
		}
		if(i%2)
		{
			posend--;
			posstart++;
		}
	}
	
	return 0;

}

int** Compute::calcStep()
{
	//Hier wird das Ganze Array durchgegangen und die einzelnen Sandk�rner 
	//je nach Nachbarn im Array verschoben
	//Die �berpr�fung geht im m_board mittels Pointer die weiterverschoben werden 
	
	for(int i=HEIGHT-1; i>=0; i--)
	{
		//Pointer um durch die Arrays durchgehen zu k�nnen und Nachbarn vergleichen zu k�nnen
		p_lefttop = &m_board[i][0];
		p_leftbot = &m_board[i+1][0];
		p_righttop = &m_board[i][1];
		p_rightbot = &m_board[i+1][1];

		
		for(int j=0; j < WIDTH; j++)
		{
			if(*p_lefttop == 2 && *p_leftbot == 0 && *p_righttop == 0 && *p_rightbot == 0)
			{
				*p_lefttop = 0;
				*p_leftbot = 2;
			}
			
			if(*p_lefttop == 0 && *p_leftbot == 0 && *p_righttop == 2 && *p_rightbot == 0)
			{
				*p_righttop = 0;
				*p_rightbot = 2;
			}
			
			if(*p_lefttop == 2 && *p_leftbot == 2 && *p_righttop == 0 && *p_rightbot == 0)
			{
				*p_lefttop = 0;
				*p_rightbot = 2;
			}
			
			if(*p_lefttop == 0 && *p_leftbot == 0 && *p_righttop == 2 && *p_rightbot == 2)
			{
				*p_righttop = 0;
				*p_leftbot = 2;
			}
			
			if(*p_lefttop == 2 && *p_leftbot == 0 && *p_righttop == 0 && *p_rightbot == 2)
			{
				*p_lefttop = 0;
				*p_leftbot = 2;
			}
			
			if(*p_lefttop == 0 && *p_leftbot == 2 && *p_righttop == 2 && *p_rightbot == 0)
			{
				*p_righttop = 0;
				*p_rightbot = 2;
			}
			
			if(*p_lefttop == 2 && *p_leftbot == 0 && *p_righttop == 0 && *p_rightbot == 0)
			{
				*p_lefttop = 0;
				*p_leftbot = 2;
			}
			
			if(*p_lefttop == 2 && *p_leftbot == 0 && *p_righttop == 2 && *p_rightbot == 2)
			{
				*p_lefttop = 0;
				*p_leftbot = 2;
			}
			
			if(*p_lefttop == 2 && *p_leftbot == 2 && *p_righttop == 2 && *p_rightbot == 0)
			{
				*p_righttop = 0;
				*p_rightbot = 2;
			}

			if(*p_lefttop == 2 && *p_leftbot == 0 && *p_righttop == 2 && *p_rightbot == 0)
			{	
				//Ist die Random Nummer h�her als 50 fallen Beide Top Sandk�rner nach unten ist Sie kleiner bleiben 
				//sie stecken
				int r = rand()% 100 + 1;
				if(r>50)
				{
					*p_leftbot = 2;
					*p_rightbot = 2;
					*p_righttop = 0;
					*p_lefttop = 0;
				}
			}

			//Pointer im Array um einen Schritt weiterschieben
			p_lefttop++;
			p_leftbot++;
			p_righttop++;
			p_rightbot++;
		}

	}

	return m_board;
}

//TestFunktion zum Ausgeben des Arrays auf der Konsole
int Compute::printUhr()
{
	for(int i=0;i<HEIGHT; i++)
	{
		for(int j=0; j<WIDTH; j++)
		{	
			//std::cout << m_board[i][j];
		}
		//std::cout << std::endl;
	}

	return  0;
}
